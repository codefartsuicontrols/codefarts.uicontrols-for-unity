﻿namespace Assets.Codefarts_Game.UIControls.Project_Files.Scripts
{
    using Codefarts.UIControls;

    using UnityEngine;

    class TextBoxInputTest : MonoBehaviour
    {
        private TextBox textBox;

        private string text2 = string.Empty;

        private string text3 = string.Empty;

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            this.textBox = new TextBox();
            this.textBox.KeyDown += textBox_KeyDown;
            this.textBox.KeyUp += textBox_KeyUp;
            this.textBox.MouseEnter += textBox_MouseEnter;
            this.textBox.MouseLeave += textBox_MouseLeave;
            this.textBox.MouseMove += textBox_MouseMove;
        }

        void textBox_MouseMove(object sender, MouseEventArgs e)
        {
            Debug.Log(string.Format("\r\nMove {0}, {1}", e.X, e.Y));
        }

        void textBox_MouseLeave(object sender, MouseEventArgs e)
        {
            Debug.Log(string.Format("\r\nLeave {0}, {1}", e.X, e.Y));
        }

        void textBox_MouseEnter(object sender, MouseEventArgs e)
        {
            Debug.Log(string.Format("\r\nEnter {0}, {1}", e.X, e.Y));
        }

        void textBox_KeyUp(object sender, KeyEventArgs e)
        {
            Debug.Log(string.Format("KeyUpEvent down: {0} up: {1} toggled: {2} key:{3})", e.IsDown, e.IsUp, e.IsToggled, e.Key));
        }

        void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            Debug.Log(string.Format("KeyDownEvent down: {0} up: {1} toggled: {2} key:{3})", e.IsDown, e.IsUp, e.IsToggled, e.Key));
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            this.textBox.MarginLeft = 100;
            this.textBox.MarginTop = 100;
            this.textBox.Width = 100;
            this.textBox.Height = 100;
            this.text3 = GUI.TextArea(new Rect(100, 150, 100, 100), this.text3);
            ControlRendererManager.Instance.DrawControl(this.textBox, Time.deltaTime, Time.time);
            this.text2 = GUI.TextField(new Rect(100, 100, 100, 25), this.text2);
        }
    }
}
