namespace Codefarts.UIControls
{
    public class ButtonCheckBox : CheckBox
    {
        public ButtonCheckBox(ImageSource image)
            : base(image)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ButtonCheckBox"/> class.
        /// </summary>
        public ButtonCheckBox()
            : base()
        {
        }
    }
}