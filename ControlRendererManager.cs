namespace Codefarts.UIControls
{
    using System;
    using System.Collections.Generic;

    using Codefarts.UIControls.Code.Renderers;
    using Codefarts.UIControls.Interfaces;
    using Codefarts.UIControls.Renderers.Unity;

    using UnityEngine;

    public class ControlRendererManager : IDisposable, IControlRendererManager
    {
        private static ControlRendererManager singleton;

        public static ControlRendererManager Instance
        {
            get
            {
                if (singleton == null)
                {
                    singleton = new ControlRendererManager();
                }

                return singleton;
            }
        }

        public GUISkin Skin { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlRendererManager"/> class.
        /// </summary>
        public ControlRendererManager()
        {
            this.controlRenderers = new Dictionary<Type, IControlRenderer>();
            this.MaximumNesting = 100000;
            this.LoadControlRendererPlugins();
        }

        public virtual void Dispose(Control control)
        {
            if (control == null || control.ExtendedProperties == null)
            {
                return;
            }

            var names = control.ExtendedProperties.GetNames();
            foreach (var name in names)
            {
                try
                {
                    var value = control.ExtendedProperties.Get(name);
                    if (value is UnityEngine.Object)
                    {
                        GameObject.Destroy((UnityEngine.Object)value);
                    }
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// Holds the list of editor tools.
        /// </summary>
        private IDictionary<Type, IControlRenderer> controlRenderers;

        public Type[] GetControlTypes()
        {
            var keys = new Type[this.controlRenderers.Count];
            this.controlRenderers.Keys.CopyTo(keys, 0);
            return keys;
        }

        public IControlRenderer Get(Type controlType)
        {
            return this.controlRenderers[controlType];
        }

        public int Count
        {
            get
            {
                return this.controlRenderers.Count;
            }
        }

        /// <summary>
        /// Unloads all previously loaded plugins.
        /// </summary>
        private void UnloadPlugins()
        {
            lock (this.controlRenderers)
            {
                this.controlRenderers.Clear();
            }
        }

        /// <summary>
        /// Loads all the tool plugins using reflection.
        /// </summary>
        private void LoadControlRendererPlugins()
        {
#if UNITY_WEBPLAYER
            this.controlRenderers.Add(typeof(ButtonCheckBoxRenderer), new ButtonCheckBoxRenderer());
            this.controlRenderers.Add(typeof(ButtonRenderer), new ButtonRenderer());
            this.controlRenderers.Add(typeof(CallbacksCustomControlRenderer), new CallbacksCustomControlRenderer());
            this.controlRenderers.Add(typeof(CheckBoxRenderer), new CheckBoxRenderer());
            this.controlRenderers.Add(typeof(ContainerRenderer), new ContainerRenderer());
            this.controlRenderers.Add(typeof(FlexibleSpaceRenderer), new FlexibleSpaceRenderer());
            this.controlRenderers.Add(typeof(ImageRenderer), new ImageRenderer());
            this.controlRenderers.Add(typeof(LabelRenderer), new LabelRenderer());
            this.controlRenderers.Add(typeof(ListBoxRenderer), new ListBoxRenderer());
            this.controlRenderers.Add(typeof(NumericTextFieldRenderer), new NumericTextFieldRenderer());
            this.controlRenderers.Add(typeof(ProgressBarRenderer), new ProgressBarRenderer());
            this.controlRenderers.Add(typeof(ScrollBarRenderer), new ScrollBarRenderer());
            this.controlRenderers.Add(typeof(ScrollViewerRenderer), new ScrollViewerRenderer());
            this.controlRenderers.Add(typeof(SliderRenderer), new SliderRenderer());
            this.controlRenderers.Add(typeof(StackPanelRenderer), new StackPanelRenderer());
            this.controlRenderers.Add(typeof(TextBoxRenderer), new TextBoxRenderer());
            this.controlRenderers.Add(typeof(TextFieldRenderer), new TextFieldRenderer());
            this.controlRenderers.Add(typeof(TreeViewRenderer), new TreeViewRenderer());
            this.controlRenderers.Add(typeof(WindowRenderer), new WindowRenderer());
#else
            // search for types in each assembly that implement the IEditorTool interface
            var list = new List<IControlRenderer>();
            var fullName = typeof(IControlRenderer).FullName;
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var type in asm.GetTypes())
                {
                    if (type.IsAbstract)
                    {
                        continue;
                    }

                    foreach (var inter in type.GetInterfaces())
                    {
                        try
                        {
                            if (string.CompareOrdinal(inter.FullName, fullName) == 0)
                            {

                                var obj = asm.CreateInstance(type.FullName);
                                var instance = obj as IControlRenderer;
                                list.Add(instance);
                            }
                        }
                        catch (Exception ex)
                        {
                            // ignore error
                            Debug.LogError(string.Format("Problem loading '{0}' as a control renderer plugin.", type.FullName));
                            Debug.LogException(ex);
                        }
                    }
                } 
            }

            foreach (var renderer in list)
            {
                try
                {
                    this.controlRenderers.Add(renderer.ControlType, renderer);
                }
                catch (Exception ex)
                {
                    // ignore error
                    Debug.LogError(string.Format("Problem adding '{0}' another renderer may have already been added.", renderer.ControlType.FullName));
                    Debug.LogException(ex);
                }
            }
#endif
        }

        public void Dispose()
        {
            try
            {
                this.UnloadPlugins();
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public int MaximumNesting
        {
            get
            {
                return this.maximumNesting;
            }

            set
            {
                this.maximumNesting = value < 0 ? 0 : value;
            }
        }

        private int drawingNestingCount;
        private int updateNestingCount;

        private int maximumNesting;

        public void DrawControl(Control control, float elapsedGameTime, float totalGameTime)
        {
            if (control == null)
            {
                return;
            }

            IControlRenderer renderer = null;
            lock (this.controlRenderers)
            {
                if (this.controlRenderers.ContainsKey(control.GetType()))
                {
                    renderer = this.controlRenderers[control.GetType()];
                }
            }

            if (renderer != null)
            {
                GUI.skin = this.Skin;
                if (this.drawingNestingCount + 1 > this.MaximumNesting)
                {
                    this.drawingNestingCount = 0;
                    throw new Exception("Exceeded maximum draw nesting count!");
                }

                try
                {
                    this.drawingNestingCount++;
                    renderer.Draw(this, control, elapsedGameTime, totalGameTime);
                }
                finally
                {
                    this.drawingNestingCount--;
                }
            }
            else
            {
                if (control is ICustomRendering)
                {
                    var custom = control as ICustomRendering;
                    custom.Draw(this, control, elapsedGameTime, totalGameTime);
                }
            }
        }

        public void UpdateControl(Control control, float elapsedGameTime, float totalGameTime)
        {
            if (control == null)
            {
                return;
            }

            IControlRenderer renderer = null;
            lock (this.controlRenderers)
            {
                if (this.controlRenderers.ContainsKey(control.GetType()))
                {
                    renderer = this.controlRenderers[control.GetType()];
                }
            }

            if (renderer != null)
            {
                if (this.updateNestingCount + 1 > this.MaximumNesting)
                {
                    this.updateNestingCount = 0;
                    throw new Exception("Exceeded maximum update nesting count!");
                }

                try
                {
                    this.updateNestingCount++;
                    renderer.Update(this, control, elapsedGameTime, totalGameTime);
                }
                finally
                {
                    this.updateNestingCount--;
                }
            }
            else
            {
                if (control is ICustomRendering)
                {
                    var custom = control as ICustomRendering;
                    custom.Update(this, control, elapsedGameTime, totalGameTime);
                }
            }
        }
    }
}