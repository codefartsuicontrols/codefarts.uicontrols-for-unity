/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace CBX.Unity.Editor.GUIDesigner
{
    using Codefarts.UIControls;
    using Codefarts.UIControls.Code;
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    public class AAA : CustomControl
    {
        public string Text { get; set; }

        public Texture2D Texture { get; set; }


        public override void OnDraw(IControlRendererManager manager, float elapsedGameTime, float totalGameTime)
        {
            var rect = GUILayoutUtility.GetLastRect();// this.GetScreenRectangle();
            this.Text = rect.ToString();
            var content = new GUIContent(this.Text, this.Texture);

            if (this.MarginLeft == 0 && this.MarginTop == 0)
            {
                if (GUILayout.Button(content, ControlDrawingHelpers.StandardDimentionOptions(this)))
                {

                }
            }
            else
            {
                if (GUI.Button(new Rect(this.MarginLeft + rect.x, this.MarginTop + rect.y, this.Width, this.Height), content))
                {

                }
            }
        }

        public override void OnUpdate(IControlRendererManager manager, float elapsedGameTime, float totalGameTime)
        {
        }
    }
}