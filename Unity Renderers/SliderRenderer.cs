﻿namespace Codefarts.UIControls.Renderers.Unity
{
    using System;

    using Codefarts.UIControls.Code.Renderers;
    using Codefarts.UIControls.Interfaces;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="Slider"/> control.
    /// </summary>
    public class SliderRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(Slider);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        /// <exception cref="System.ArgumentException">Argument does not inherit from Slider.;control</exception>
        public override  void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        { 
            var slider = control as Slider; 
          
            float value;
            switch (slider.Orientation)
            {
                case Orientation.Horizontial:
                    value = GUILayout.HorizontalSlider(slider.Value, slider.Minimum, slider.Maximum, ControlDrawingHelpers.StandardDimentionOptions(slider));
                    if (slider.IsEnabled)
                    {
                        slider.Value = value;
                    }

                    break;

                case Orientation.Vertical:
                    value = GUILayout.VerticalSlider(slider.Value, slider.Minimum, slider.Maximum, ControlDrawingHelpers.StandardDimentionOptions(slider));
                    if (slider.IsEnabled)
                    {
                        slider.Value = value;
                    }

                    break;
            }
        } 
    }
}