﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces;
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="Window"/> control.
    /// </summary>
    public class WindowRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(Window);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        { 
            var window = control as Window;
            var windowRect = new Rect(control.MarginLeft, control.MarginTop, control.Width, control.Height);

            windowRect = GUILayout.Window(
                window.ID,
                windowRect,
                id =>
                {
                    foreach (var child in window.Children)
                    {
                        manager.DrawControl(child, elapsedGameTime, totalGameTime);
                    }

                    if (window.IsDragable)
                    {
                        GUI.DragWindow();
                    }
                },
                window.Title,
               ControlDrawingHelpers.StandardDimentionOptions(control));
                    // handle mouse enter & leave events
            this.HandleMouseEvents(control);
    control.MarginLeft = windowRect.x;
            control.MarginTop = windowRect.y;
            control.Width = windowRect.width;
            control.Height = windowRect.height;

        } 
    }
}