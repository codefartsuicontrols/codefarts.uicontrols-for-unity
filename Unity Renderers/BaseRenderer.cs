﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces;

    using UnityEngine;

    /// <summary>
    /// Provides a base implementation of the <see cref="IControlRenderer"/> interface.
    /// </summary>
    public abstract class BaseRenderer : IControlRenderer
    {
        /// <summary>
        /// The cached mouse arguments to prevent additional garbage collection.
        /// </summary>
        private MouseEventArgs mouseArguments;

        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public abstract Type ControlType { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRenderer"/> class.
        /// </summary>
        public BaseRenderer()
        {
            this.mouseArguments = new MouseEventArgs();
        }

        /// <summary>
        /// Draws the specified control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be rendered.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        /// <exception cref="System.InvalidCastException">If the type of control does not match the <see cref="ControlType"/> property.</exception>
        /// <remarks>This base renderer implementation will handle visibility and enabled state management, as well as throw exceptions if 
        /// the provided control is null or not of the expected type.</remarks>
        public void Draw(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            //if(control.GetType().GetNestedType(this.ControlType.FullName)==null)
            //{

            //}

            //if (control.GetType() != this.ControlType )
            //{
            //    throw new InvalidCastException(string.Format("'control' is not of type '{0}.", this.ControlType.FullName));
            //}

            if (control.Visibility != Visibility.Visible)
            {
                return;
            }

            var prevState = GUI.enabled;
            GUI.enabled = prevState && control.IsEnabled;
            this.DrawControl(manager, control, elapsedGameTime, totalGameTime);
            GUI.enabled = prevState;
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public abstract void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime);

        /// <summary>
        /// Updates the specified control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        /// <remarks>
        /// <p>This method is provided if a control has is animated and it's animation state can be updated independently of drawing.</p>
        /// <p>Updates generally occur more frequently then draws.</p>
        /// <p>This base renderer implementation will throw exceptions if the provided control is null or not of the expected type. </p>
        /// </remarks>
        public virtual void Update(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            if (control.GetType() != this.ControlType)
            {
                throw new InvalidCastException(string.Format("'control' is not of type '{0}.", this.ControlType.FullName));
            }
        }

        protected void HandleKeyEventsAfterControlDrawn(string controlName, KeyCode keyCode, bool isDown, Control control, bool isUp)
        {
            if (GUI.GetNameOfFocusedControl() == controlName && keyCode != KeyCode.None)
            {
                if (isDown)
                {
                    control.OnKeyDown(new KeyEventArgs(keyCode, isDown, isUp));
                }

                if (isUp)
                {
                    control.OnKeyUp(new KeyEventArgs(keyCode, isDown, isUp));
                }
            }
        }

        protected void HandleKeyEventsBeforeControlDrawn(Control control, out KeyCode keyCode, out bool isDown, out bool isUp, out string controlName)
        {
            var current = Event.current;
            keyCode = KeyCode.None;
            isDown = false;
            isUp = false;
            if (control.IsEnabled)
            {
                if (current.isKey && current.keyCode != KeyCode.None && current.type == EventType.KeyDown)
                {
                    keyCode = current.keyCode;
                    isDown = true;
                }

                if (current.isKey && current.keyCode != KeyCode.None && current.type == EventType.KeyUp)
                {
                    keyCode = current.keyCode;
                    isUp = true;
                }
            }

            if (control.ExtendedProperties == null)
            {
                control.ExtendedProperties = new PropertyCollection();
            }

            controlName = control.ExtendedProperties.ContainsName("tbName - 27F4D1B5-3E93-4092-AA87-A653E63218F1")
                              ? control.ExtendedProperties["tbName - 27F4D1B5-3E93-4092-AA87-A653E63218F1"].ToString()
                              : null;
            if (string.IsNullOrEmpty(controlName))
            {
                controlName = Guid.NewGuid().ToString();
                control.ExtendedProperties["tbName - 27F4D1B5-3E93-4092-AA87-A653E63218F1"] = controlName;
            }

            GUI.SetNextControlName(controlName);
        }

        /// <summary>
        /// Handles the <see cref="Control.MouseEnter"/> & <see cref="Control.MouseLeave"/> events.
        /// </summary>
        /// <param name="control">The control to check.</param>
        protected void HandleMouseEvents(Control control)
        {
            var current = Event.current;
            if (current.type == EventType.Repaint)
            {
                var leftButton = Input.GetMouseButtonDown(0);
                var rightButton = Input.GetMouseButtonDown(1);
                var middleButton = Input.GetMouseButtonDown(2);
                var button1 = Input.GetMouseButtonDown(3);
                var button2 = Input.GetMouseButtonDown(4);

                var lastRect = GUILayoutUtility.GetLastRect();
                var relativeMousePosition = current.mousePosition - lastRect.min;
                var isOver = lastRect.Contains(current.mousePosition);
                var isMouseOver = control.IsMouseOver;
                var props = control.ExtendedProperties;
                if (props == null)
                {
                    props = new PropertyCollection();
                    control.ExtendedProperties = props;
                }

                props[Control.IsMouseOverKey] = isOver;
                var previousMousePosition = new Vector2(int.MinValue, int.MinValue);
                if (props.ContainsName(Control.PreviousMousePositionKey))
                {
                    previousMousePosition = (Vector2)props[Control.PreviousMousePositionKey];
                }

                if (!isMouseOver && isOver)
                {
                    this.mouseArguments.LeftButton = leftButton;
                    this.mouseArguments.RightButton = rightButton;
                    this.mouseArguments.MiddleButton = middleButton;
                    this.mouseArguments.XButton1 = button1;
                    this.mouseArguments.XButton2 = button2;
                    this.mouseArguments.X = relativeMousePosition.x;
                    this.mouseArguments.Y = relativeMousePosition.y;
                    control.OnMouseEnter(this.mouseArguments);
                }

                if (isOver && previousMousePosition != current.mousePosition)
                {
                    this.mouseArguments.LeftButton = leftButton;
                    this.mouseArguments.RightButton = rightButton;
                    this.mouseArguments.MiddleButton = middleButton;
                    this.mouseArguments.XButton1 = button1;
                    this.mouseArguments.XButton2 = button2;
                    this.mouseArguments.X = relativeMousePosition.x;
                    this.mouseArguments.Y = relativeMousePosition.y;
                    control.OnMouseMove(this.mouseArguments);
                    props[Control.PreviousMousePositionKey] = current.mousePosition;
                }

                if (isMouseOver && !isOver)
                {
                    this.mouseArguments.LeftButton = leftButton;
                    this.mouseArguments.RightButton = rightButton;
                    this.mouseArguments.MiddleButton = middleButton;
                    this.mouseArguments.XButton1 = button1;
                    this.mouseArguments.XButton2 = button2;
                    this.mouseArguments.X = relativeMousePosition.x;
                    this.mouseArguments.Y = relativeMousePosition.y;
                    control.OnMouseLeave(this.mouseArguments);
                }
            }
        }
    }
}