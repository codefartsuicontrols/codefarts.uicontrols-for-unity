﻿namespace Codefarts.UIControls.Renderers.Unity
{
    using System;

    using Codefarts.UIControls.Code.Renderers;
    using Codefarts.UIControls.Interfaces;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="ScrollBar"/> control.
    /// </summary>
    public class ScrollBarRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(ScrollBar);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        /// <exception cref="System.ArgumentException">Argument does not inherit from ScrollBar.;control</exception> 
        public override  void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        { 
            var scrollBar = control as ScrollBar; 
            switch (scrollBar.Orientation)
            {
                case Orientation.Horizontial:
                    scrollBar.Value = GUILayout.HorizontalScrollbar(scrollBar.Value, 1, scrollBar.Minimum, scrollBar.Maximum, ControlDrawingHelpers.StandardDimentionOptions(scrollBar));
                    break;

                case Orientation.Vertical:
                    scrollBar.Value = GUILayout.VerticalScrollbar(scrollBar.Value, 1, scrollBar.Minimum, scrollBar.Maximum, ControlDrawingHelpers.StandardDimentionOptions(scrollBar));
                    break; 
            }
        } 
    }
}