﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces;
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="CheckBox"/> control.
    /// </summary>
    public class CheckBoxRenderer : BaseRenderer
    {
        /// <summary>
        /// The content for the check box.
        /// </summary>
        private GUIContent content;

        /// <summary>
        /// The styleHOlds a reference to a <see cref="GUIStyle"/> that will be used to draw the check box.
        /// </summary>
        protected GUIStyle style;

        /// <summary>
        /// Initializes a new instance of the <see cref="ButtonRenderer"/> class.
        /// </summary>
        public CheckBoxRenderer()
        {
            this.content = new GUIContent();
        }

        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(CheckBox);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var checkBox = control as CheckBox;

            if (this.style == null)
            {
                this.style = GUI.skin.toggle;
            }

            this.content.text = string.IsNullOrEmpty(checkBox.Text) ? string.Empty : checkBox.Text;
            if (checkBox.Image is Texture2DSource)
            {
                this.content.image = ((Texture2DSource)checkBox.Image).Texture;
            }

            if (control.MarginLeft == 0 && control.MarginTop == 0)
            {
                checkBox.IsChecked = GUILayout.Toggle(checkBox.IsChecked, this.content, this.style, ControlDrawingHelpers.StandardDimentionOptions(control));
            }
            else
            {
                checkBox.IsChecked = GUI.Toggle(new Rect(control.MarginLeft, control.MarginTop, control.Width, control.Height), checkBox.IsChecked, this.content, this.style);
            }
        }
    }
}