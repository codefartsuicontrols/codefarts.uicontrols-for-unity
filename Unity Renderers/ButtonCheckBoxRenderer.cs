﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="ButtonCheckBox"/> control.
    /// </summary>
    public class ButtonCheckBoxRenderer : CheckBoxRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(ButtonCheckBox);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            // if style has not yet been set set it to a button style.
            if (this.style != GUI.skin.button)
            {
                this.style = GUI.skin.button;
            }

            base.DrawControl(manager, control, elapsedGameTime, totalGameTime);
        }
    }
}