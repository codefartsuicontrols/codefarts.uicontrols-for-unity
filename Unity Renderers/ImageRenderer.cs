﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Controls;
    using Codefarts.UIControls.Interfaces;
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="Image"/> control.
    /// </summary>
    public class ImageRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(Image);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var image = control as Image;
            if (image.Source is Texture2DSource)
            {
                var source = image.Source as Texture2DSource;

                var scale = ScaleMode.StretchToFill;
                switch (image.Stretch)
                {
                    case Stretch.Fill:
                        scale = ScaleMode.StretchToFill;
                        break;

                    case Stretch.None:
                        scale = ScaleMode.ScaleAndCrop;
                        break;

                    case Stretch.Uniform:
                        scale = ScaleMode.ScaleToFit;
                        break;

                    case Stretch.UniformToFill:
                        scale = ScaleMode.StretchToFill;
                        break;
                }

                // figure out dimensions
                var width = control.Width == 0 ? control.MinWidth : control.Width;
                var height = control.Height == 0 ? control.MinHeight : control.Height;

                switch (scale)
                {
                    case ScaleMode.StretchToFill:
                        break;

                    case ScaleMode.ScaleAndCrop:
                        break;

                    case ScaleMode.ScaleToFit:
                        width = width == 0 ? image.Source.Width : width;
                        height = height == 0 ? image.Source.Height : height;
                        break;
                }

                var rect = GUILayoutUtility.GetRect(width, height);
                //var rect = new Rect(control.MarginLeft, control.MarginTop, width, height);
                GUI.DrawTexture(rect, source.Texture, scale, true);
            }
        } 
    }
}