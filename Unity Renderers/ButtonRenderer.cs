﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces;
    using Codefarts.UIControls.Unity;

    using UnityEngine;
    using UnityEngine.UI;

    using Button = Codefarts.UIControls.Button;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> for the <see cref="UIControls.Button"/> control.
    /// </summary>
    public class ButtonRenderer : BaseRenderer
    {
        /// <summary>
        /// The content for the button.
        /// </summary>
        private GUIContent content;

        public const string MainGameObjectKey = "F8E891C2-104A-4658-977A-7DD3BF14B4EE";
        public const string GameObjectLabelKey = "003DC85E-014E-419B-832D-3D553B6767D1";
        public const string MainRectTransformKey = "695D901C-F763-44F0-816D-38CE0EEA2B28";

        /// <summary>
        /// Initializes a new instance of the <see cref="ButtonRenderer"/> class.
        /// </summary>
        public ButtonRenderer()
        {
            this.content = new GUIContent();
        }

        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(Button);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var button = control as Button;
#if LEGACYUI
            this.content.text = button.Text;
            this.content.image = null;
            if (button.Image is Texture2DSource)
            {
                this.content.image = ((Texture2DSource)button.Image).Texture;
            }

            if (control.MarginLeft == 0 && control.MarginTop == 0)
            {
                if (GUILayout.Button(this.content, ControlDrawingHelpers.StandardDimentionOptions(control)))
                {
                    button.OnClick();
                }

                // handle mouse enter & leave events
                this.HandleMouseEvents(control);
            }
            else
            {
                if (GUI.Button(new Rect(control.MarginLeft, control.MarginTop, control.Width, control.Height), this.content))
                {
                    button.OnClick();
                }
            }


#else

            // create extended properties if not already existing
            if (control.ExtendedProperties == null)
            {
                // we don't draw the control here as a design decision
                // because we don't want to take control over someones code.
                // for example someone else may be explicitly setting extended properties to null
                // but we would then be creating the extended properties again causing perf hit 
                // and a conflict of interests. 
                return;
            }

            // get the unity button game object if it exists or create it
            GameObject gameObject;
            if (!control.ExtendedProperties.TryGet(MainGameObjectKey, out gameObject))
            {
                gameObject = new GameObject("UnityButton") { layer = LayerMask.NameToLayer("UI") };
                control.ExtendedProperties.Add(MainGameObjectKey, gameObject);
            }

            // create rect transform if it does not already exist
            RectTransform rectTransform;
            if (!control.ExtendedProperties.TryGet(MainRectTransformKey, out rectTransform))
            {
                rectTransform = gameObject.AddComponent<RectTransform>();
                control.ExtendedProperties.Add(MainRectTransformKey, rectTransform);
            }

            // set rect transform properties
            rectTransform.pivot = Vector2.zero;
            rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, control.MarginLeft, 0);
            rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, control.MarginTop, 0);
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, control.Width);
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, control.Height);

            // ensure main game object has a canvas renderer
            var canvasRenderer = gameObject.GetComponent<CanvasRenderer>();
            canvasRenderer = canvasRenderer == null ? gameObject.AddComponent<CanvasRenderer>() : canvasRenderer;
            
            // make sure main canvas has image
            var imageComponent = gameObject.GetComponent<Image>();
            imageComponent = canvasRenderer == null ? gameObject.AddComponent<Image>() : imageComponent;
            
            // set image properties
            imageComponent.type = Image.Type.Sliced;
            
            // make sure image has a sprite assigned and if not assign the default sprite
            if (imageComponent.sprite == null)
            {
                Sprite sprite = null;
                foreach (var item in Resources.FindObjectsOfTypeAll<Sprite>())
                {
                    if (item.name == "UISprite")
                    {
                        sprite = item;
                        break;
                    }
                }

                imageComponent.sprite = sprite;
            }

            // make sure main object has button component
            var buttonComponent = gameObject.GetComponent<UnityEngine.UI.Button>();
            buttonComponent = buttonComponent == null ? gameObject.AddComponent<UnityEngine.UI.Button>() : buttonComponent;
            
            // add listen er to call buttons OnClick method when clicked.
            buttonComponent.onClick.AddListener(button.OnClick);

            GameObject label;// = new GameObject("Text") { layer = LayerMask.NameToLayer("UI") };
            if (!control.ExtendedProperties.TryGet(GameObjectLabelKey, out label))
            {
                label = new GameObject("Text") { layer = LayerMask.NameToLayer("UI") };
                control.ExtendedProperties.Add(GameObjectLabelKey, gameObject);
            }
            var textRectTransform = label.GetComponent<RectTransform>();
            textRectTransform = textRectTransform == null ? label.AddComponent<RectTransform>() : textRectTransform;
            textRectTransform.anchorMin = Vector2.zero;
            textRectTransform.anchorMax = Vector2.one;

            canvasRenderer = label.GetComponent<CanvasRenderer>();
            canvasRenderer = canvasRenderer == null ? label.AddComponent<CanvasRenderer>() : canvasRenderer;

            var textComponent = label.GetComponent<Text>();
            textComponent = textComponent == null ? label.AddComponent<Text>() : textComponent;
            if (label.transform.parent == null)
            {
                label.transform.SetParent(buttonComponent.transform, false);
            }

            var canvas = GameObject.FindObjectOfType<Canvas>();
            gameObject.transform.SetParent(canvas.transform, false);
            this.Font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
            this.TextColor = Color.gray;
            this.TextAlignment = TextAnchor.MiddleCenter;
#endif
        }
    }
}
