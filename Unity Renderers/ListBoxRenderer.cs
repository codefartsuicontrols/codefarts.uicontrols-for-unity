﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces;

#if UNITY3D
    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="ListBox"/> control.
    /// </summary>
    public class ListBoxRenderer : BaseRenderer
    {
        /// <summary>
        /// Holds a cached <see cref="ListBoxItemInformationArgs"/> reference.
        /// </summary>
        ListBoxItemInformationArgs measureItemArgs = new ListBoxItemInformationArgs();

        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(ListBox);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var listbox = control as ListBox;
            var scrollPosition = new Vector2(listbox.HorizontialOffset, listbox.VerticalOffset);
            if (listbox.HorizontialScrollBarVisibility == ScrollBarVisibility.Auto &
                listbox.VerticalScrollBarVisibility == ScrollBarVisibility.Auto)
            {
                scrollPosition = GUILayout.BeginScrollView(scrollPosition, ControlDrawingHelpers.StandardDimentionOptions(control));
            }
            else
            {
                scrollPosition = GUILayout.BeginScrollView(
                    scrollPosition,
                    listbox.HorizontialScrollBarVisibility == ScrollBarVisibility.Visible,
                    listbox.VerticalScrollBarVisibility == ScrollBarVisibility.Visible,
                    GUI.skin.horizontalScrollbar, GUI.skin.verticalScrollbar, GUI.skin.box,
                 ControlDrawingHelpers.StandardDimentionOptions(control));
            }

            var items = new object[listbox.Items.Count];
            listbox.Items.CopyTo(items, 0);

            switch (listbox.DrawMode)
            {
                case DrawMode.Normal:
                    listbox.SelectedIndex = this.SelectionList(manager, control, elapsedGameTime, totalGameTime, listbox.SelectedIndex, items, dblClickCallback => { });
                    break;

                case DrawMode.OwnerDraw:
                    var sizes = new Vector2[items.Length];

                    // measure all items and store there sizes in the sizes array
                    for (var i = 0; i < items.Length; i++)
                    {
                        this.measureItemArgs.Item = items[i];
                        this.measureItemArgs.Index = i;
                        listbox.OnMeasureItem(this.measureItemArgs);
                        sizes[i] = new Vector2(this.measureItemArgs.ItemWidth, this.measureItemArgs.ItemHeight);
                    }

                    // draw all the items
                    for (var i = 0; i < items.Length; i++)
                    {
                        this.measureItemArgs.Item = items[i];
                        this.measureItemArgs.Index = i;
                        var size = sizes[i];
                        this.measureItemArgs.ItemWidth = size.x;
                        this.measureItemArgs.ItemHeight = size.y;
                        listbox.OnDrawItem(this.measureItemArgs);
                    }

                    break;
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndScrollView();
            listbox.HorizontialOffset = scrollPosition.x;
            listbox.VerticalOffset = scrollPosition.y;
        }

        private int SelectionList(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime, int selectedIndex,
            object[] list, Action<int> doubleClickCallback)
        {
            for (var i = 0; i < list.Length; ++i)
            {
                var entry = list[i];
                if (entry == null)
                {
                    continue;
                }

                if (entry is ICustomRendering)
                {
                    var renderer = entry as ICustomRendering;
                    // renderer.Draw(manager,null,);
                }
                else
                {
                    var guiContent = entry is GUIContent ? (GUIContent)entry : new GUIContent(entry.ToString());
                    var elementStyle = i == selectedIndex ? control.GetSelectedListBoxEntryStyle() : control.GetListBoxEntryStyle();
                    var elementRect = GUILayoutUtility.GetRect(guiContent, elementStyle);

                    var current = Event.current;
                    var hover = elementRect.Contains(current.mousePosition);
                    if (hover && current.type == EventType.MouseDown && current.clickCount == 1)
                    {
                        selectedIndex = i;
                        current.Use();
                    }
                    // check if changed from MouseUp to MouseDown
                    else if (hover && doubleClickCallback != null && current.type == EventType.MouseDown && current.clickCount == 2)
                    {
                        // Debug.Log("Works !");
                        doubleClickCallback(i);
                        current.Use();
                    }
                    else if (current.type == EventType.repaint)
                    {
                        elementStyle.Draw(elementRect, guiContent, hover, false, i == selectedIndex, false);
                    }
                }
            }

            return selectedIndex;
        }
    }
#endif
}