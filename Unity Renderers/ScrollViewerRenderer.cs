﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="ScrollViewer"/> control.
    /// </summary>
    public class ScrollViewerRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(ScrollViewer);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var viewer = control as ScrollViewer;
            var scrollPosition = new Vector2(viewer.HorizontialOffset, viewer.VerticalOffset);
            try
            {
                if (viewer.HorizontialScrollBarVisibility == ScrollBarVisibility.Auto &
                   viewer.VerticalScrollBarVisibility == ScrollBarVisibility.Auto)
                {
                    scrollPosition = GUILayout.BeginScrollView(scrollPosition, ControlDrawingHelpers.StandardDimentionOptions(control));
                }
                else
                {
                   // Debug.Log("here " + viewer.HorizontialScrollBarVisibility.ToString());
                    scrollPosition = GUILayout.BeginScrollView(
                        scrollPosition,
                        viewer.HorizontialScrollBarVisibility == ScrollBarVisibility.Visible,
                        viewer.VerticalScrollBarVisibility == ScrollBarVisibility.Visible,
                    ControlDrawingHelpers.StandardDimentionOptions(control));
                }

                foreach (var child in viewer.Children)
                {
                    manager.DrawControl(child, elapsedGameTime, totalGameTime);
                }
            }
            finally
            {
                // GUILayout.FlexibleSpace();
                GUILayout.EndScrollView();
            }

            viewer.HorizontialOffset = scrollPosition.x;
            viewer.VerticalOffset = scrollPosition.y;
        }
    }
}