﻿namespace Codefarts.UIControls.Renderers.Unity
{
    using System;

    using Codefarts.UIControls.Code.Renderers;
    using Codefarts.UIControls.Interfaces;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="Slider"/> control.
    /// </summary>
    public class ProgressBarRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(ProgressBar);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        /// <exception cref="System.ArgumentException">Argument does not inherit from Slider.;control</exception>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var containerControl = control as ProgressBar;
            var style = control.GetBackgroundBrushStyle();
            var foregroundStyle = control.GetForegroundBrushStyle();

            var useBeginArea = containerControl.MarginLeft != 0 || containerControl.MarginTop != 0;
            if (useBeginArea)
            {
                GUILayout.BeginArea(new Rect(containerControl.MarginLeft, containerControl.MarginTop, containerControl.Width, containerControl.Height));
            }
            
            var rect = GUILayoutUtility.GetRect(0, 10000, 0, 10000, style == null ? GUI.skin.box : style, ControlDrawingHelpers.StandardDimentionOptions(control));
           // Debug.Log(rect);
            rect.width = ((containerControl.Value - containerControl.Minimum) / (containerControl.Maximum - containerControl.Minimum)) * rect.width;
          //  Debug.Log("after " + rect);
         
            // rect.width = (containerControl.Value / containerControl.Maximum) * rect.width;
            GUI.Box(rect, string.Empty, foregroundStyle == null ? GUI.skin.box : foregroundStyle);

            if (useBeginArea)
            {
                GUILayout.EndArea();
            } 
            
            return;
            //var rect = GUILayoutUtility.GetRect(GUIContent.none, style, ControlDrawingHelpers.StandardDimentionOptions(control));

            if (style != null)
            {
                GUILayout.BeginVertical(style, ControlDrawingHelpers.StandardDimentionOptions(control));
            }
            else
            {
                GUILayout.BeginVertical(ControlDrawingHelpers.StandardDimentionOptions(control));
            }

            try
            {

            }
            finally
            {
                GUILayout.EndVertical();
            }

            var progressBar = control as ProgressBar;

            float value;
            switch (progressBar.Orientation)
            {
                case Orientation.Horizontial:
                    GUILayout.BeginHorizontal();

                    GUILayout.EndHorizontal();
                    value = GUILayout.HorizontalSlider(progressBar.Value, progressBar.Minimum, progressBar.Maximum, ControlDrawingHelpers.StandardDimentionOptions(progressBar));
                    if (progressBar.IsEnabled)
                    {
                        progressBar.Value = value;
                    }

                    break;

                case Orientation.Vertical:
                    value = GUILayout.VerticalSlider(progressBar.Value, progressBar.Minimum, progressBar.Maximum, ControlDrawingHelpers.StandardDimentionOptions(progressBar));
                    if (progressBar.IsEnabled)
                    {
                        progressBar.Value = value;
                    }

                    break;
            }
        }
    }
}