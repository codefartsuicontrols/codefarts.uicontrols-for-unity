﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces; 

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="NumericTextField"/> control.
    /// </summary>
    public class NumericTextFieldRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(NumericTextField);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        { 
            var textField = control as NumericTextField;
            // var controlId = GUIUtility.GetControlID(FocusType.Passive) + 1;
            var value = GUILayout.TextField(string.IsNullOrEmpty(textField.Text) ? string.Empty : textField.Text, ControlDrawingHelpers.StandardDimentionOptions(control));
            if (control.IsEnabled)
            {
                //textField.Text = this.SanitizeValue(value, textField.Minimum, textField.Maximum);
                textField.Text = value;
            } 
        } 
    }
}