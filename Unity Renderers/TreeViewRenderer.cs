﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;
    using System.Collections.Generic;

    using Codefarts.UIControls.Interfaces;
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="TreeView"/> control.
    /// </summary>
    public class TreeViewRenderer : BaseRenderer
    {
        /// <summary>
        /// Caches the selection style.
        /// </summary>
        private static GUIStyle selectionStyle;

        /// <summary>
        /// Caches a normal style.
        /// </summary>
        private GUIStyle normalStyle;

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeViewRenderer"/> class.
        /// </summary>
        public TreeViewRenderer()
        {
            // auto register this control type with the control drawing service
            // var service = UnityControlRenderingService.Instance;
            //  service.Register<TreeView>(ControlDrawingHelpers.RenderCustomControl);
            if (selectionStyle == null)
            {
                selectionStyle = new GUIStyle("label");
                selectionStyle.name = "TreeViewSelection";
            }

            this.Inset = 8;
        }

        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(TreeView);
            }
        }

        /// <summary>
        /// Gets or sets the inset value that controls how far inset tree nodes are drawn.
        /// </summary> 
        /// <remarks>The default value is 8.</remarks>
        public float Inset { get; set; }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        { 
            var tree = control as TreeView;
            var stack = new Stack<TreeViewNode>();
            try
            {
                var scroll = GUILayout.BeginScrollView(new Vector2(tree.HorizontialOffset, tree.VerticalOffset), true, true, ControlDrawingHelpers.StandardDimentionOptions(tree));
                tree.HorizontialOffset = scroll.x;
                tree.VerticalOffset = scroll.y;
                foreach (var node in tree.Nodes)
                {
                    this.DrawNode(tree, manager, stack, node, elapsedGameTime, totalGameTime);
                }
            }
            finally
            {
                GUILayout.EndScrollView();
            }

            // handle mouse enter & leave events
            this.HandleMouseEvents(control);
        }

        private void DrawNode(TreeView tree, IControlRendererManager manager, Stack<TreeViewNode> stack, TreeViewNode node, float elapsedGameTime, float totalGameTime)
        {
            if (!node.IsVisible)
            {
                return;
            }

            if (selectionStyle == null)
            {
                selectionStyle = new GUIStyle(GUI.skin.label);
                selectionStyle.normal.textColor = GUI.skin.settings.selectionColor;
                selectionStyle.wordWrap = false;
            }

            if (this.normalStyle != GUI.skin.label)
            {
                this.normalStyle = new GUIStyle(GUI.skin.label) { wordWrap = false };
            }

            var nodeList = node.Nodes;
            try
            {
                GUILayout.BeginHorizontal();

                GUILayout.Space(stack.Count * (node.Nodes != null && node.Nodes.Count == 0 ? this.Inset * 2 : this.Inset));
                stack.Push(node);

                if (nodeList != null && nodeList.Count > 0 && GUILayout.Button(node.IsExpanded ? "-" : "+", this.normalStyle))
                {
                    node.IsExpanded = !node.IsExpanded;
                }


                if (node.Value is Control)
                {
                    manager.DrawControl(node.Value as Control, elapsedGameTime, totalGameTime);
                }                          
                else
                {
                    var label = tree.SelectedNode == node ? selectionStyle : normalStyle;
                    var button = GUILayout.Button(node.Value.ToString(), label);
                    tree.SelectedNode = button ? node : tree.SelectedNode;
                }

                GUILayout.FlexibleSpace();
            }
            finally
            {
                GUILayout.EndHorizontal();
            }

            if (node.IsExpanded && nodeList != null)
            {
                foreach (var child in nodeList)
                {
                    this.DrawNode(tree, manager, stack, child, elapsedGameTime, totalGameTime);
                }
            }

            stack.Pop();
        } 
    }
}