﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces;

#if UNITY3D
    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="StackPanel"/> control.
    /// </summary>
    public class StackPanelRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(StackPanel);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var panel = control as StackPanel;

            var style = control.GetBackgroundBrushStyle();

            var useBeginArea = panel.MarginLeft != 0 || panel.MarginTop != 0;
            if (useBeginArea)
            {
                GUILayout.BeginArea(new Rect(panel.MarginLeft, panel.MarginTop, panel.Width, panel.Height));
            }

            if (panel.Orientation == Orientation.Horizontial)
            {
                if (style != null)
                {
                    //  Debug.Log("clipping " + style.clipping.ToString());
                   // style.clipping = TextClipping.Clip;

                    GUILayout.BeginHorizontal(style, ControlDrawingHelpers.StandardDimentionOptions(control));
                }
                else
                {
                    GUILayout.BeginHorizontal(ControlDrawingHelpers.StandardDimentionOptions(control));
                }
            }
            else
            {
                if (style != null)
                {
                    GUILayout.BeginVertical(style, ControlDrawingHelpers.StandardDimentionOptions(control));
                }
                else
                {
                    GUILayout.BeginVertical(ControlDrawingHelpers.StandardDimentionOptions(control));
                }
            }

            //if (panel.Orientation == Orientation.Horizontial)
            //{
            //    if (style != null)
            //    {
            //        //  Debug.Log("clipping " + style.clipping.ToString());
            //        style.clipping = TextClipping.Clip;

            //        GUILayout.BeginHorizontal(style, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));//, ControlDrawingHelpers.StandardDimentionOptions(control));
            //    }
            //    else
            //    {
            //        GUILayout.BeginHorizontal(GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));//ControlDrawingHelpers.StandardDimentionOptions(control));
            //    }
            //}
            //else
            //{
            //    if (style != null)
            //    {
            //        GUILayout.BeginVertical(style, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));//, ControlDrawingHelpers.StandardDimentionOptions(control));
            //    }
            //    else
            //    {
            //        GUILayout.BeginVertical(GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));//ControlDrawingHelpers.StandardDimentionOptions(control));
            //    }
            //}
            try
            {
                Control[] children;
                lock (panel.Children)
                {
                    children = new Control[panel.Children.Count];
                    panel.Children.CopyTo(children, 0);
                }

                if (children != null)
                {
                    // TODO: Horrible code is this contains a large set of items extreme slow down
                    Array.Sort(
                        children,
                        (x, y) =>
                        {
                            var indexX = Array.IndexOf(children, x);
                            var indexY = Array.IndexOf(children, y);

                            if (x is IControlGroup)
                            {
                                indexX = (x as IControlGroup).Group;
                            }

                            if (indexX < indexY)
                            {
                                return -1;
                            }

                            if (indexX > indexY)
                            {
                                return 1;
                            }

                            return 0;
                        });

                    var doClip = control.ClipToBounds;
                    string groupId = null;
                    if (doClip)
                    {
                        groupId = control.ExtendedProperties.ContainsName("BeginGroup") ? (string)control.ExtendedProperties["BeginGroup"] : Guid.NewGuid().ToString();
                        control.ExtendedProperties["BeginGroup"] = groupId;
                        // Debug.Log(groupId);  
                        GUILayoutUtility.BeginGroup(groupId);
                    }

                    foreach (var child in children)
                    {
                        manager.DrawControl(child, elapsedGameTime, totalGameTime);
                    }

                    if (doClip)
                    {
                        groupId = (string)control.ExtendedProperties["BeginGroup"];
                        GUILayoutUtility.EndGroup(groupId);
                    }
                }
            }
            finally
            {
                if (panel.Orientation == Orientation.Horizontial)
                {
                    GUILayout.EndHorizontal();
                }
                else
                {
                    GUILayout.EndVertical();
                }
            }

            if (useBeginArea)
            {
                GUILayout.EndArea();
            }

            // handle mouse enter & leave events
            this.HandleMouseEvents(control);
        }
    }
#endif
}