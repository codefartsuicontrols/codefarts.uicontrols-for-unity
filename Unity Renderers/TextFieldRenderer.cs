﻿namespace Codefarts.UIControls.Code.Renderers
{
#if UNITY3D
    using System;

    using Codefarts.UIControls.Interfaces;
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="TextField"/> control.
    /// </summary>
    public class TextFieldRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(TextField);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var textField = control as TextField;
            var value = textField.Text;

            switch (textField.Visibility)
            {
                case Visibility.Visible:
                    KeyCode keyCode;
                    bool isDown;
                    bool isUp;
                    string controlName;
                    this.HandleKeyEventsBeforeControlDrawn(control, out keyCode, out isDown, out isUp, out controlName);

                    value = GUILayout.TextField(string.IsNullOrEmpty(textField.Text) ? string.Empty : textField.Text, ControlDrawingHelpers.StandardDimentionOptions(control));

                    if (control.IsEnabled)
                    {
                        textField.Text = value;
                    }
                  
                    if (GUI.changed)
                    {
                        textField.Text = value;
                    }

                    this.HandleKeyEventsAfterControlDrawn(controlName, keyCode, isDown, textField, isUp);

                    // handle mouse enter & leave events
                    this.HandleMouseEvents(control);

                    break;

                case Visibility.Hidden:
                    if (Event.current.type == EventType.Layout)
                    {
                        value = GUILayout.TextField(string.IsNullOrEmpty(textField.Text) ? string.Empty : textField.Text, ControlDrawingHelpers.StandardDimentionOptions(control));
                    }

                    break;
            }
        }
    }
#endif
}