﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="CallbacksCustomControl"/>.
    /// </summary>
    public class CallbacksCustomControlRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(CallbacksCustomControl);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var callbacks = control as CallbacksCustomControl;
            if (callbacks != null)
            {
                callbacks.Draw(manager, elapsedGameTime, totalGameTime);
            }
        }

        /// <summary>
        /// Updates the specified control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        /// <remarks>
        /// <p>This method is provided if a control has is animated and it's animation state can be updated independently of drawing.</p>
        /// <p>Updates generally occur more frequently then draws.</p>
        /// <p>This base renderer implementation will throw exceptions if the provided control is null or not of the expected type. </p>
        /// </remarks>
        public override void Update(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var callbacks = control as CallbacksCustomControl;
            if (callbacks != null)
            {
                callbacks.Update(manager, elapsedGameTime, totalGameTime);
            }
        }
    }
}                                                                                 