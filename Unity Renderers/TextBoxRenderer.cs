﻿namespace Codefarts.UIControls.Code.Renderers
{
#if UNITY3D
    using System;

    using Codefarts.UIControls.Interfaces;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="TextBox"/> control.
    /// </summary>
    public class TextBoxRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(TextBox);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var textBox = control as TextBox;
            var text = textBox.Text == null ? string.Empty : textBox.Text;

            switch (control.Visibility)
            {
                case Visibility.Visible:
                    var scrollPosition = new Vector2(textBox.HorizontalOffset, textBox.VerticalOffset);
                    if (textBox.HorizontalScrollBarVisibility != ScrollBarVisibility.Hidden | textBox.VerticalScrollBarVisibility != ScrollBarVisibility.Hidden)
                    {
                        scrollPosition = GUILayout.BeginScrollView(
                            scrollPosition,
                            textBox.HorizontalScrollBarVisibility == ScrollBarVisibility.Visible,
                            textBox.VerticalScrollBarVisibility == ScrollBarVisibility.Visible);
                    }

                    var value = textBox.Text;

                    KeyCode keyCode;
                    bool isDown;
                    bool isUp;
                    string controlName;
                    this.HandleKeyEventsBeforeControlDrawn(control, out keyCode, out isDown, out isUp, out controlName);

                    if (textBox.AcceptsReturn)
                    {
                        value = GUILayout.TextArea(text, ControlDrawingHelpers.StandardDimentionOptions(control));
                    }
                    else
                    {
                        value = GUILayout.TextField(text, ControlDrawingHelpers.StandardDimentionOptions(control));
                    }


                    if (GUI.changed)
                    {
                        textBox.Text = value;
                    }

                    this.HandleKeyEventsAfterControlDrawn(controlName, keyCode, isDown, textBox, isUp);

                    // handle mouse enter & leave events
                    this.HandleMouseEvents(control);


                    if (textBox.HorizontalScrollBarVisibility != ScrollBarVisibility.Hidden || textBox.VerticalScrollBarVisibility != ScrollBarVisibility.Hidden)
                    {
                        GUILayout.EndScrollView();
                        textBox.HorizontalOffset = scrollPosition.x;
                        textBox.VerticalOffset = scrollPosition.y;
                    } break;

                case Visibility.Hidden:
                    if (Event.current.type == EventType.Layout)
                    {
                        value = GUILayout.TextArea(text, ControlDrawingHelpers.StandardDimentionOptions(control));
                    }

                    break;
            }
        }


    }
#endif

}