﻿namespace Codefarts.UIControls.Code.Renderers
{
    using Codefarts.UIControls.Interfaces;
#if UNITY3D
    using UnityEngine;
    using System;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="Label"/> control.
    /// </summary>
    public class LabelRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(Label);
            }
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var label = control as Label;
            GUILayout.Label(label.Text, ControlDrawingHelpers.StandardDimentionOptions(control));
        }
    }
#endif
}