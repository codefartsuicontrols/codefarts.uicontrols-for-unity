﻿namespace Codefarts.UIControls.Unity
{
    using System;

    using UnityEngine;

    public class Texture2DSource : ImageSource
    {
        /// <summary>
        /// The metadata information for the texture.
        /// </summary>
        private Texture2DMetadata metadata;

        /// <summary>
        /// The texture reference for this texture source.
        /// </summary>
        private Texture2D texture;

        /// <summary>
        /// Initializes a new instance of the <see cref="Texture2DSource"/> class.
        /// </summary>
        public Texture2DSource()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Texture2DSource"/> class.
        /// </summary>
        /// <param name="texture2D">The texture to be associated with this texture source.</param>
        public Texture2DSource(Texture2D texture2D)
            : this()
        {
            if (texture2D == null)
            {
                throw new ArgumentNullException("texture2D");
            }

            this.texture = texture2D;
            this.metadata = new Texture2DMetadata(texture2D);
        }

        /// <summary>
        /// Gets or sets the texture.
        /// </summary>                
        public virtual Texture2D Texture
        {
            get
            {
                return this.texture;
            }

            set
            {
                var changed = this.texture != value;
                if (!changed)
                {
                    return;
                }

                this.texture = value;
                this.metadata = new Texture2DMetadata(value);
            }
        }

        /// <summary>
        /// Gets the image height.
        /// </summary>
        public override int Height
        {
            get
            {
                return this.Texture == null ? 0 : this.Texture.height;
            }
        }

        /// <summary>
        /// Gets the image width.
        /// </summary>
        public override int Width
        {
            get
            {
                return this.Texture == null ? 0 : this.Texture.width;
            }
        }

        /// <summary>
        /// Gets the metadata for the image source.
        /// </summary>    
        public override ImageMetadata Metadata
        {
            get
            {
                return this.metadata;
            }
        }
    }
}