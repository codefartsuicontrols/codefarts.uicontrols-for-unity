/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.UIControls
{
    using System;
    using System.Net.Mime;

    using Codefarts.UIControls.Interfaces;
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    using Vector2 = UnityEngine.Vector2;

    /// <summary>
    /// Provides methods for drawing controls.
    /// </summary>
    public static class ControlDrawingHelpers
    {
        private const string BackgroundSolidBrushStyleKey = "851A7F0D-0761-47D9-B786-5AE4E54DD90C";
        private const string ForegroundSolidBrushStyleKey = "BC6F78A9-8BCD-4DAB-8EE8-9024F7266A9A";
        private const string BackgroundImageBrushStyleKey = "6F95C63D-2815-4E9F-8775-49FDA314E52F";
        private const string ForegroundImageBrushStyleKey = "A06AFF33-E8F1-4CA2-8C73-32AA57E32CCB";
        private const string ListBoxEntryStyleKey = "A3EA10C1-AEB0-4CDF-9E7A-1186970EDD96";
        private const string SelectedListBoxEntryStyleKey = "6DAB9181-0994-4D4E-9BEB-0AA0367BD639";

        public static GUIStyle GetListBoxEntryStyle(this Control control)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            var style = control.GetStyle(ListBoxEntryStyleKey, false);
            if (style == null)
            {
                style = new GUIStyle(GUI.skin.label);
                control.ExtendedProperties[ListBoxEntryStyleKey] = style;
            }

            return style;
        }

        public static GUIStyle GetSelectedListBoxEntryStyle(this Control control)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            var style = control.GetStyle(SelectedListBoxEntryStyleKey, false);
            if (style == null)
            {
                style = new GUIStyle(GUI.skin.label);

                var texture = new Texture2D(4, 4, TextureFormat.ARGB32, false);
                //TODO: setting texture pixeles here is slow dont do it
                texture.SetPixels(
                    new[]
                        {
                            Color.blue,Color.blue,Color.blue,Color.blue,
                            Color.blue,Color.blue,Color.blue,Color.blue,
                            Color.blue,Color.blue,Color.blue,Color.blue,
                            Color.blue,Color.blue,Color.blue,Color.blue
                        });

                texture.Apply();
                style.normal.background = texture;
                control.ExtendedProperties[SelectedListBoxEntryStyleKey] = style;
            }

            return style;
        }

        public static GUIStyle GetBackgroundBrushStyle(this Control control)
        {
            GUIStyle style = null;
            var brush = control.Background;
            if (brush != null)
            {
                if (brush is SolidColorBrush)
                {
                    return GetBrushStyle(control, brush, BackgroundSolidBrushStyleKey);
                }

                if (brush is ImageBrush)
                {
                    return GetBrushStyle(control, brush, BackgroundImageBrushStyleKey);
                }
            }

            return style;
        }

        public static GUIStyle GetForegroundBrushStyle(this Control control )
        {
            GUIStyle style = null;
            var brush = control.Foreground;
            if (brush != null)
            {
                if (brush is SolidColorBrush)
                {
                    return GetBrushStyle(control, brush, ForegroundSolidBrushStyleKey);
                }

                if (brush is ImageBrush)
                {
                    return GetBrushStyle(control, brush, ForegroundImageBrushStyleKey);
                }
            }

            return style;
        }

        private static GUIStyle GetBrushStyle(this Control control, Brush brush, string key)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            GUIStyle style = null;
            if (brush != null)                                                                                       
            {
                if (brush is SolidColorBrush)
                {
                    var solidBrush = brush as SolidColorBrush;
                    var color = solidBrush.Color;
                    color.a = solidBrush.Opacity;
                    style = control.GetStyle(key, true);

                    if (style.normal.background == null)
                    {
                        style.normal.background = new Texture2D(4, 4, TextureFormat.ARGB32, false);
                    }
                                               //TODO: setting texture pixeles here is slow dont do it
                    style.normal.background.SetPixels(
                               new[] { 
                                   color, color, color, color, 
                                   color, color, color, color, 
                                   color, color, color, color, 
                                   color, color, color, color 
                               });
                    style.normal.background.Apply();
                }
                else if (brush is ImageBrush)
                {
                    var imageBrush = brush as ImageBrush;
                    if (imageBrush.Image is Texture2DSource)
                    {
                        style = control.GetStyle(key, true);
                        style.normal.background = ((Texture2DSource)imageBrush.Image).Texture;
                    }
                }
            }

            return style;
        }

        public static GUIStyle GetStyle(this Control control, string key, bool? create)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            var properties = control.ExtendedProperties;
            if (properties == null)
            {
                properties = new PropertyCollection();
                control.ExtendedProperties = properties;
            }

            GUIStyle style;
            if (!properties.TryGet(key, out style) || style == null)
            {
                if (create.HasValue && create.Value)
                {
                    style = new GUIStyle();
                    properties[key] = style;
                }
            }

            return style;
        }

        /// <summary>
        /// Gets the screen rectangle for the control.
        /// </summary>
        /// <param name="control">A reference to the control to get the screen rectangle for.</param>
        /// <returns>Returns the screen rectangle for the control.</returns>
        public static Rect GetScreenRectangle(this Control control)
        {
            //var a = GUIUtility.GUIToScreenPoint(new Vector2(control.Left, control.Top));
            //var b = GUIUtility.GUIToScreenPoint(new Vector2(control.Left + control.Width, control.Top + control.Height));
            //return new Rect(a.x, a.y, b.x - a.x, b.y - a.y);

            var screenMin = GUIUtility.ScreenToGUIPoint(new Vector2(0, 0));
            var screenMax = GUIUtility.ScreenToGUIPoint(new Vector2(Screen.width, Screen.height));

            // top left corner in screen coordinates
            var tl = Vector2.zero - screenMin;

            // get bottom right corner in screen coordinates
            var br = screenMax - (tl + new Vector2(control.Width, control.Height));

            br = GUIUtility.GUIToScreenPoint(br - tl);
            tl = GUIUtility.GUIToScreenPoint(tl);
            return new Rect(tl.x, tl.y, br.x - tl.x, br.y - tl.y);
        }

        /// <summary>
        /// Returns an array of <see cref="GUILayoutOption"/> types based on the state of the control.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public static GUILayoutOption[] StandardDimentionOptions(Control control)
        {
            // check if null reference
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            var items = new GUILayoutOption[8];
            var count = 0;
            if (control.MinWidth > 0)
            {
                items[count++] = GUILayout.MinWidth(control.MinWidth);
            }

            if (control.MinHeight > 0)
            {
                items[count++] = GUILayout.MinHeight(control.MinHeight);
            }

            if (control.MaxWidth != float.MaxValue)
            {
                items[count++] = GUILayout.MaxWidth(control.MaxWidth);
            }

            if (control.MaxHeight != float.MaxValue)
            {
                items[count++] = GUILayout.MaxHeight(control.MaxHeight);
            }

            if (control.Width != 0)
            {
                //  Debug.Log("width");
                items[count++] = GUILayout.Width(control.Width);
            }

            if (control.Height != 0)
            {
                //   Debug.Log("height");
                items[count++] = GUILayout.Height(control.Height);
            }

            if (control.Width == 0 && control.MinWidth == 0f && control.MaxWidth == float.MaxValue)
            {
                items[count++] = GUILayout.ExpandWidth(control.HorizontalAlignment == HorizontalAlignment.Stretch);
            }

            if (control.Height == 0 && control.MinHeight == 0f && control.MaxHeight == float.MaxValue)
            {
                items[count++] = GUILayout.ExpandHeight(control.VerticalAlignment == VerticalAlignment.Stretch);
            }

            Array.Resize(ref items, count);
            return items;
        }

        public static int SelectionList(int selected, object[] list, Action<int> callback)
        {
            return SelectionList(selected, list, "List Item", callback);
        }

        public static int SelectionList(int selected, object[] list, GUIStyle elementStyle, Action<int> callback)
        {
            for (var i = 0; i < list.Length; ++i)
            {
                var entry = list[i];
                if (entry == null)
                {
                    continue;
                }

                var guiContent = entry is GUIContent ? (GUIContent)entry : new GUIContent(entry.ToString());
                var elementRect = GUILayoutUtility.GetRect(guiContent, elementStyle);

                var current = Event.current;
                var hover = elementRect.Contains(current.mousePosition);
                if (hover && current.type == EventType.MouseDown && current.clickCount == 1)
                {
                    selected = i;
                    current.Use();
                }
                // check if changed from MouseUp to MouseDown
                else if (hover && callback != null && current.type == EventType.MouseDown && current.clickCount == 2)
                {
                    Debug.Log("Works !");
                    callback(i);
                    current.Use();
                }
                else if (current.type == EventType.repaint)
                {
                    elementStyle.Draw(elementRect, guiContent, hover, false, i == selected, false);
                }
            }

            return selected;
        }
        //#region List box

        /// <summary>
        /// Provides a delegate
        /// </summary>
        /// <param name="index"></param>
        // public delegate void DoubleClickCallback(int index);

        //public static int SelectionList(int selected, GUIContent[] list)
        //{
        //    return SelectionList(selected, list, "List Item", null);
        //}

        //public static int SelectionList(int selected, GUIContent[] list, GUIStyle elementStyle)
        //{
        //    return SelectionList(selected, list, elementStyle, null);
        //}

        //public static int SelectionList(int selected, GUIContent[] list, DoubleClickCallback callback)
        //{
        //    return SelectionList(selected, list, "List Item", callback);
        //}

        //public static int SelectionList(int selected, GUIContent[] list, GUIStyle elementStyle, DoubleClickCallback callback)
        //{
        //    for (int i = 0; i < list.Length; ++i)
        //    {
        //        Rect elementRect = GUILayoutUtility.GetRect(list[i], elementStyle);
        //        bool hover = elementRect.Contains(Event.current.mousePosition);
        //        if (hover && Event.current.type == EventType.MouseDown && Event.current.clickCount == 1) // added " && Event.current.clickCount == 1"
        //        {
        //            selected = i;
        //            Event.current.Use();
        //        }
        //        else if (hover && callback != null && Event.current.type == EventType.MouseDown && Event.current.clickCount == 2) //Changed from MouseUp to MouseDown
        //        {
        //            Debug.Log("Works !");
        //            callback(i);
        //            Event.current.Use();
        //        } 


        //        else if (Event.current.type == EventType.repaint)
        //        {
        //            elementStyle.Draw(elementRect, list[i], hover, false, i == selected, false);
        //        }
        //    }
        //    return selected;
        //}

        //public static int SelectionList(int selected, string[] list)
        //{
        //    return SelectionList(selected, list, "List Item", null);
        //}

        //public static int SelectionList(int selected, string[] list, GUIStyle elementStyle)
        //{
        //    return SelectionList(selected, list, elementStyle, null);
        //}

        //public static int SelectionList(int selected, string[] list, DoubleClickCallback callback)
        //{
        //    return SelectionList(selected, list, "List Item", callback);
        //}

        //public static int SelectionList(int selected, string[] list, GUIStyle elementStyle, DoubleClickCallback callback)
        //{
        //    for (int i = 0; i < list.Length; ++i)
        //    {
        //        Rect elementRect = GUILayoutUtility.GetRect(new GUIContent(list[i]), elementStyle);
        //        bool hover = elementRect.Contains(Event.current.mousePosition);
        //        if (hover && Event.current.type == EventType.MouseDown && Event.current.clickCount == 1) // added " && Event.current.clickCount == 1"
        //        {
        //            selected = i;
        //            Event.current.Use();
        //        }
        //        else if (hover && callback != null && Event.current.type == EventType.MouseDown && Event.current.clickCount == 2) //Changed from MouseUp to MouseDown
        //        {
        //            Debug.Log("Works !");
        //            callback(i);
        //            Event.current.Use();
        //        }
        //        else if (Event.current.type == EventType.repaint)
        //        {
        //            elementStyle.Draw(elementRect, list[i], hover, false, i == selected, false);
        //        }
        //    }
        //    return selected;
        //}
        //#endregion

    }
}
